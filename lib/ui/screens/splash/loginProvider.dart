import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'package:HirohataCargo/src/helper/LoginHelper.dart';
import 'package:HirohataCargo/src/models/glitch/glitch.dart';
import 'package:HirohataCargo/src/services/httpConfig.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';

class LoginProvider with ChangeNotifier {
  bool loading = false;
  final _helper = LoginHelper();
  final _streamController = StreamController<Either<Glitch, bool>>();

  Stream<Either<Glitch, bool>> get loginStrem {
    return _streamController.stream;
  }

  Future<void> login(String user, String password) async {
    final loginResult = await _helper.login(user, password);
    _streamController.add(loginResult);
  }

  _changeLoadingState(bool state) {
    loading = state;
    notifyListeners();
  }

  void clear() {
    loading = false;
    notifyListeners();
  }
}

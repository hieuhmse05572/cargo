import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/glitch/NoInternetGlitch.dart';
import 'package:HirohataCargo/src/models/order.dart';
import 'package:HirohataCargo/src/utils/AppConfig.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/ui/screens/order/order.dart';
import 'package:HirohataCargo/ui/screens/order/orderProvider.dart';
import 'package:HirohataCargo/ui/screens/splash/loginProvider.dart';
import 'package:after_layout/after_layout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

import 'fadeScreen.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with AfterLayoutMixin {
  final provider = getIt<LoginProvider>();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        color: Color(0xFFF5F5F5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 200,
              decoration: BoxDecoration(
                  image: DecorationImage(
                image: AssetImage("assets/icon/icon.png"),
                fit: BoxFit.contain,
              )),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 20),
              alignment: Alignment.topCenter,
              child: Text(
                'Hirohata Cargo',
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
                child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 50,
                    width: 1,
                  ),
                  CupertinoActivityIndicator(
                    radius: 14,
                  ),
                  SizedBox(
                    height: 6,
                    width: 1,
                  ),
                  Text(
                    'ローディング',
                    style: TextStyle(color: Colors.grey),
                  )
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    provider.login("00005", "12345678");
    provider.loginStrem.listen((snapshot) {
      snapshot.fold((l) {
        if (l is NoInternetGlitch) {
          Toast.show(Msg.Timeout, context,
              duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        }
      }, (r) {
        Toast.show(Msg.LoginSucess, context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (context, animation, anotherAnimation) {
              return OrderScreen();
            },
            transitionDuration: Duration(milliseconds: 700),
            transitionsBuilder: (context, animation, anotherAnimation, child) {
              animation = CurvedAnimation(
                  curve: Curves.fastOutSlowIn, parent: animation);
              return Align(
                  child: FadeTransition(
                opacity: animation,
                child: child,
              ));
            }));
        // Route route = FadeRoute(page: OrderScreen());
        // Navigator.pushReplacement(context, route);
      });
    });
  }
}

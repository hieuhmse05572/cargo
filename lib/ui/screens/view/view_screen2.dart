import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/ui/painter/background.dart';
import 'package:HirohataCargo/ui/screens/edit/edit.dart';
import 'package:HirohataCargo/ui/screens/order/order.dart';
import 'package:HirohataCargo/ui/screens/view/viewScreenProvider.dart';
import 'package:HirohataCargo/ui/widgets/dialog.dart';
import 'package:HirohataCargo/ui/widgets/loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import '../../painter/renderBox.dart';

class ViewScreen2 extends StatefulWidget {
  final String order;
  ViewScreen2({this.order});
  @override
  _ViewScreenState createState() => _ViewScreenState();
}

class _ViewScreenState extends State<ViewScreen2> {
  final provider = getIt<ViewProvider>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    provider.getShapes(widget.order);
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () async {
          provider.reset();
          return true;
        },
        child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                onPressed: () {
                  Navigator.of(context).pushReplacement(PageRouteBuilder(
                      pageBuilder: (context, animation, anotherAnimation) {
                        return OrderScreen();
                      },
                      transitionDuration: Duration(milliseconds: 500),
                      transitionsBuilder:
                          (context, animation, anotherAnimation, child) {
                        animation = CurvedAnimation(
                            curve: Curves.easeIn, parent: animation);
                        return Align(
                            child: FadeTransition(
                          opacity: animation,
                          child: child,
                        ));
                      }));
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                ),
              ),
              title: Text(
                "${provider.order}",
                style: TextStyle(color: Colors.white),
              ),
              actions: [
                IconButton(
                  onPressed: () {
                    Navigator.of(context).pushReplacement(PageRouteBuilder(
                        pageBuilder: (context, animation, anotherAnimation) {
                          return EditScreen(
                            order: widget.order,
                            truck: provider.truck
                          );
                        },
                        transitionDuration: Duration(milliseconds: 500),
                        transitionsBuilder:
                            (context, animation, anotherAnimation, child) {
                          animation = CurvedAnimation(
                              curve: Curves.easeIn, parent: animation);
                          return Align(
                              child: FadeTransition(
                            opacity: animation,
                            child: child,
                          ));
                        }));
                  },
                  icon: Icon(Icons.edit, color: Colors.white),
                )
              ],
            ),
            body: getStream(_getStack(context))));
  }

  // Widget _getLoading() {
  //   return StreamBuilder<bool>(
  //       stream: provider.streamLoading,
  //       initialData: false,
  //       builder: (context, snapshot) {
  //         bool loading = snapshot.data;
  //         return loading
  //             ? Stack(
  //                 children: [
  //                   _getNote(context),
  //                   innerLoading(),
  //                   _getBackground(context)
  //                 ],
  //               )
  //             : getStream(_getStack(context));
  //       });
  // }

  Widget getStream(List<Widget> stack) {
    return StreamBuilder<List<Widget>>(
        stream: provider.streamShapes,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Widget> widgetsOnStack = List();
            widgetsOnStack.addAll(stack);
            widgetsOnStack.addAll(snapshot.data);
            widgetsOnStack.add(_getGestureDetector(context));
            widgetsOnStack.add(_getButtonChangeCar(context));
            return Stack(
              children: widgetsOnStack,
            );
            // return ListView.builder(
            //   itemCount: snapshot.data.length,
            //   itemBuilder: (context, index) {
            //     return Text(snapshot.data[index]);
            //   },
            // );
          } else {
            return Stack(
              children: [
                _getNoteStream(),
                innerLoading(),
                _getBackground(context)
              ],
            );
          }
        });
  }
}

List<Widget> _getStack(BuildContext context) {
  List<Widget> widgetsOnStack = List();

  widgetsOnStack.add(_getBackground(context));
  widgetsOnStack.add(_getNoteStream());

  // widgetsOnStack.add(_getGestureDetector(context));
  // widgetsOnStack.add(_getButtonChangeCar(context));
  // widgetsOnStack.add(MyHomePage());
  // widgetsOnStack.add(Positioned(
  //     right: 80,
  //     bottom: 20,
  //     child: Text('${(provider.scaleZoom * 100).roundToDouble() / 100}x')));

  return widgetsOnStack;
}

Widget _getButtonChangeCar(BuildContext context) {
  final provider = getIt<ViewProvider>();

  return Positioned(
      right: 10,
      bottom: 20,
      child: Container(
          child: ClipOval(
        child: Material(
          color: Colors.grey.withOpacity(0.6), // button color
          child: InkWell(
            splashColor: Colors.blueGrey, // inkwell color
            child: SizedBox(width: 56, height: 56, child: _getIconStream()),
            onTap: () async {
              String truckId = await DialogUtil.showDialogChangeTruck(context);
              String result = await provider.changeTypeTruck(truckId);
              if (result == "error") {
                DialogUtil.showMessages(context, Messages.CHANGE_TRUCK);
              } else if (result == "close") {
              } else if (result == "true") {
                Toast.show(Messages.SAVE_SUCCESS, context,
                    duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
              } else {}
            },
          ),
        ),
      )));
}

Widget _getIconStream() {
  final provider = getIt<ViewProvider>();

  return StreamBuilder<bool>(
      stream: provider.streamLoading,
      builder: (context, snapshot) {
        return snapshot.data == true
            ? CupertinoActivityIndicator(radius: 10)
            : Icon(
                Icons.local_car_wash,
                color: Colors.white,
              );
      });
}

Widget _getNoteStream() {
  final provider = getIt<ViewProvider>();

  return StreamBuilder<bool>(
      stream: provider.streamLoading,
      builder: (context, snapshot) {
        return _getNote();
      });
}

Widget _getNote() {
  final provider = getIt<ViewProvider>();
  return Positioned(
      left: 10,
      top: 10,
      child: Container(
        padding: EdgeInsets.all(5),
        alignment: Alignment.topLeft,
        height: 260,
        width: 160,
        child: Column(
          children: [
            CustomPaint(
              painter: InforBox1(),
            ),
            CustomPaint(
              painter: InforBox2(),
            ),
            CustomPaint(
              painter: InforBox3(),
            ),
            CustomPaint(
              painter:
                  InforColor(provider.truck.name, provider.getMaxRowNumber()),
            ),
          ],
        ),
        decoration: new BoxDecoration(
            color: Colors.grey.withOpacity(0.2),
            border: new Border.all(
              color: Colors.grey.withOpacity(0.6),
            ),
            borderRadius: BorderRadius.all(Radius.circular(5))),
      ));
}

Widget _getGestureDetector(BuildContext context) {
  final provider = getIt<ViewProvider>();

  return GestureDetector(
      onTapDown: (detail) {
        provider.onTapDown(detail.localPosition);
      },
      onScaleStart: (details) {
        provider.onScaleStart(details.focalPoint);
      },
      onScaleUpdate: (details) {
        provider.onScaleUpdate(details);
      },
      onScaleEnd: (details) {});
}

Widget _getBackground(BuildContext context) {
  final provider = getIt<ViewProvider>();

  return Positioned(
    top: provider.rootOffset.dy,
    left: provider.rootOffset.dx,
    child: CustomPaint(
        // size: Size.infinite,
        painter: BackgroundPainter(
            Size(2500, 2000), provider.scaleZoom, provider.rootOffset)),
  );
}

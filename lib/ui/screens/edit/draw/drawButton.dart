import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:flutter/material.dart';

class DrawButton extends StatelessWidget {
  final provider = getIt<DrawProvider>();

  @override
  Widget build(BuildContext context) {
    return Positioned(
        bottom: 10,
        right: 10,
        child: Row(
          children: [
            InkWell(
              onTap: () {
                provider.clear();
              },
              splashColor: Colors.grey,
              child: Container(
                width: 50,
                height: 50,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: [
                    Icon(Icons.clear, color: Colors.blue),
                    Text('クリア',
                        style: TextStyle(fontSize: 10, color: Colors.blue))
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                provider.undo();
              },
              splashColor: Colors.grey,
              child: Container(
                width: 50,
                height: 50,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: [
                    _getIconStream(),
                    Text('取り消す',
                        style: TextStyle(fontSize: 10, color: Colors.blue))
                  ],
                ),
              ),
            ),
            SizedBox(
              width: 10,
              height: 1,
            ),
          ],
        ));
  }

  Widget _getIconStream() {
    return StreamBuilder<List<Offset>>(
        stream: provider.streamPoints,
        builder: (context, snapshot) {
          return Icon(Icons.undo,
              color: provider.toUndo.length > 0 ? Colors.blue : Colors.grey);
        });
  }
}

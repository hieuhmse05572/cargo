import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:HirohataCargo/src/helper/AdditionInforHelper.dart';
import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/src/utils/AppConfig.dart';
import 'package:HirohataCargo/src/utils/NetworkUtil.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/src/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'dart:ui' as ui;

class DrawProvider with ChangeNotifier {
  // Infor infor;
  GlobalKey globalKey;
  bool initLoading = true;
  Infor infor = Infor("", "", "", false, "", "", false);
  // Infor("A1", "", Constants.drawing, true, "manbo", "kobaku", false);
  bool loading = true;
  bool hadClear = false;
  Uint8List imageOfDraw = null;
  List<Offset> points = <Offset>[];
  List<Offset> temp = <Offset>[];
  bool hadSave = false;
  List<int> toUndo = List();
  int startPoint;
  Infor initInfor;
  final _streamInfor = StreamController<Infor>.broadcast();
  Stream<Infor> get streamInfor => _streamInfor.stream;
  final _streamPoints = StreamController<List<Offset>>.broadcast();
  Stream<List<Offset>> get streamPoints => _streamPoints.stream;

  final _additionInforHelper = AdditionInforHelper();

  clear() {
    hadClear = true;
    points = <Offset>[];
    toUndo = <int>[];
    _streamPoints.add(points);
  }

  Future<void> getInfor(String order) async {
    final inforHelperResult =
        await _additionInforHelper.getAdditionInfor(order);
    inforHelperResult.fold((l) => null, (r) {
      infor = r;
      initInfor = Infor.clone(infor);
      _streamInfor.add(infor);
      points.addAll(getListList());
      _streamPoints.add(points);
    });
  }

  Future<Uint8List> capturePng() async {
    try {
      RenderRepaintBoundary boundary =
          globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);

      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      imageOfDraw = byteData.buffer.asUint8List();
    } catch (e) {
      print(e);
    }
    return null;
  }

  onPanUpdate(DragUpdateDetails details) {
    if (details.localPosition.dx < 0 ||
        details.localPosition.dx > SizeConfig.screenWidth / 13 * 5) return;
    if (details.localPosition.dy < 165) return;
    // Offset _localPosition = object.globalToLocal(details.globalPosition);
    points = new List.from(points)..add(details.localPosition);
    _streamPoints.add(points);
  }

  onPanEnd() {
    startPoint = points.length - startPoint;
    toUndo.add(startPoint);
    points.add(null);
  }

  toJson() {
    String json = "";
    points.forEach((element) {
      if (element != null) {
        double dx = element.dx;
        double dy = element.dy;
        json += "\{ 'dx':$dx, 'dy': $dy \} ,";
      } else {
        json += "\{ 'dx':-1.0, 'dy': -1.0 \} ,";
      }
    });
    json = Util.removeLastCharacter(json);
    String drawing = "[ " + json + "]";
    infor.drawing = drawing;
  }

  getListList() {
    List<Offset> points = <Offset>[];
    if (infor.drawing != "") {
      infor.drawing = infor.drawing.replaceAll("\'", "\"");
      final data = jsonDecode(infor.drawing);
      if (data != null && data.length > 0) {
        for (Map i in data) {
          Offset point = Offset(i["dx"], i["dy"]);
          if (point.dx == -1.0 && point.dy == -1.0) {
            points.add(null);
          } else
            points.add(point);
        }
      }
    }
    return points;
  }

  undo() {
    if (toUndo.length > 0) {
      temp = <Offset>[];
      temp.addAll(points);
      int length = temp.length;
      int rangeUndo = toUndo.last;
      temp.removeRange(length - rangeUndo - 1, length);
      toUndo.removeLast();
      points = <Offset>[];
      points.addAll(temp);
      _streamPoints.add(points);
    }
  }

  reset() {
    points = <Offset>[];
    imageOfDraw = null;
    toUndo = List();
    hadClear = false;
    hadSave = false;
  }

  init() {
    infor = Infor(infor.productCode, "", "", false, "", "", false);
  }

  updateCheckBox(int checkbox, bool value) {
    if (checkbox == 1)
      infor.sutashon = value;
    else
      infor.hakka_oroshi = value;
    _streamInfor.add(infor);
  }

  updateInput(int text, String content) {
    if (text == 1)
      infor.manbo = content;
    else
      infor.kobaku = content;
    _streamInfor.add(infor);
  }

  checkChangedInfo() {
    // toJson();
    if (infor.compareTo(initInfor)) return false;
    return true;
  }

  Future<bool> save() async {
    String body = "";
    toJson();
    body = infor.toJson();
    final result = await _additionInforHelper.updateAdditionInfor(body);
    return result.fold((l) {
    }, (r) {
      initInfor = Infor.clone(infor);
      // toUndo = List();
      // _streamPoints.add(points);
      hadSave = true;
      return true;
    });
  }
}

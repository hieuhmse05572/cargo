import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/truck.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/ui/screens/edit/drag/dragProvider.dart';
import 'package:HirohataCargo/ui/screens/edit/input/inputPad.dart';
import 'package:HirohataCargo/ui/screens/edit/table/table.dart';
import 'package:HirohataCargo/ui/screens/edit/input/InforInput.dart';
import 'package:HirohataCargo/ui/screens/view/view_screen2.dart';
import 'package:HirohataCargo/ui/widgets/dialog.dart';
import 'package:HirohataCargo/ui/widgets/loading.dart';
import 'package:after_layout/after_layout.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';

import 'drag/drag_pad.dart';
import 'draw/drawProvider.dart';
import 'input/InforCheckBox.dart';
import 'draw/drawPad.dart';

class EditScreen extends StatefulWidget {
  final String order;
  final Truck truck;
  EditScreen({this.order, this.truck});
  @override
  _EditScreenState createState() => _EditScreenState();
}

class _EditScreenState extends State<EditScreen> with AfterLayoutMixin {
  final provider = getIt<DragProvider>();
  final drawProvider = getIt<DrawProvider>();

  GlobalKey _globalKey = new GlobalKey();

  bool inside = false;
  Uint8List imageInMemory;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: Scaffold(
          // resizeToAvoidBottomInset : false,

          appBar: PreferredSize(
              preferredSize: Size.fromHeight(0.0),
              child: AppBar(
                automaticallyImplyLeading: false, // hides leading widget
                flexibleSpace: Container(),
              )),
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Column(
              children: [
                Expanded(
                  child: TableWidget(
                    order: widget.order,
                  ),
                  flex: 2,
                ),
                Expanded(
                  flex: 4,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: DottedBorder(
                            color: Colors.grey,
                            strokeWidth: 2,
                            child: DrawWidget(
                              order: widget.order,
                            )),
                        flex: 5,
                      ),
                      Expanded(
                          flex: 6,
                          child: GestureDetector(
                            child: Container(
                              height: double.infinity,
                              width: double.infinity,
                              child: DottedBorder(
                                  color: Colors.grey,
                                  strokeWidth: 2,
                                  child: Stack(
                                    children: [
                                      RepaintBoundary(
                                          key: _globalKey,
                                          child: DragPad(order: widget.order)),
                                      _getDragMenu(context),
                                    ],
                                  )),
                            ),
                          )),
                      Expanded(flex: 2, child: InputPad(order: widget.order))
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }

  Widget _getDragMenu(BuildContext context) {
    return Positioned(
      right: 0,
      top: 0,
      child: Container(
        decoration: new BoxDecoration(
            color: Colors.grey.withOpacity(0.1),
            // border: new Border.all(
            //   color: Colors.grey.withOpacity(0.6),
            // ),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Row(
          children: [
            InkWell(
              focusColor: Colors.blue,
              onTap: () async {
                if (provider.canSave()) {
                  await DialogUtil.showMessages(
                      context, Messages.WARNING_HEIGHT);
                  return;
                }
                int confirm = await DialogUtil.showDialogConfirm(
                    context, Messages.CONFIRM_SAVE);
                if (confirm == 0) return;
                bool statusSaveShapes = await provider.save();
                bool statusSaveInfor = await drawProvider.save();
                if (statusSaveShapes && statusSaveInfor) {
                  Toast.show(Messages.SAVE_SUCCESS, context,
                      duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                }
              },
              splashColor: Colors.grey,
              child: Container(
                width: 50,
                height: 50,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: [
                    Icon(
                      Icons.save,
                      color: Colors.purple,
                    ),
                    Text(
                      Button.btnSave,
                      style: TextStyle(fontSize: 10, color: Colors.purple),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () async {
                int confirm = await DialogUtil.showDialogConfirm(
                    context, Messages.CONFIRM_RESET);
                if (confirm == 0) return;
                await provider.getResetShape();
                bool status = await provider.save();
                if (status) {
                  Toast.show(Messages.RESET_SUCCESS, context,
                      duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                }
              },
              child: Container(
                width: 50,
                height: 50,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: [
                    Icon(Icons.restore, color: Colors.blue),
                    Text(
                      Button.btnReset,
                      style: TextStyle(fontSize: 10, color: Colors.blue),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                provider.undo();
              },
              splashColor: Colors.grey,
              child: Container(
                width: 50,
                height: 50,
                padding: EdgeInsets.all(5),
                child: Column(
                  children: [
                    _getIconStream(),
                    Text(
                      Button.btnUndo,
                      style: TextStyle(fontSize: 10, color: Colors.blue),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getIconStream() {
    return StreamBuilder<List<Widget>>(
        stream: provider.streamShapes,
        builder: (context, snapshot) {
          return Icon(Icons.undo,
              color: provider.undoBox.length > 0 ? Colors.blue : Colors.grey);
        });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    provider.globalKey = _globalKey;
    provider.truck = widget.truck;
  }
}

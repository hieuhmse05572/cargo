import 'dart:async';
import 'dart:collection';
import 'dart:math';
import 'dart:typed_data';
import 'package:HirohataCargo/src/helper/ShapeHelper.dart';
import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/models/truck.dart';
import 'package:HirohataCargo/src/utils/AppConfig.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/src/utils/utils.dart';
import 'package:HirohataCargo/ui/painter/genWidget.dart';
import 'package:HirohataCargo/ui/painter/path.dart';
import 'package:HirohataCargo/ui/painter/renderDashed.dart';
import 'package:HirohataCargo/ui/painter/renderTruck.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:ui' as ui;

import '../../../../src/utils/AppConfig.dart';

class DragProvider with ChangeNotifier {
  bool loading = true;
  List<Widget> items = [];
  List<Widget> trucks = [];
  List<Metal> metals = [];
  List<Metal> initMetals = [];
  List<Point> dashLines = [];
  String order;
  bool addVerticalWoodChanged = false;
  Truck truck = Constants.truckContainer;
  GlobalKey globalKey;

  // Infor infor;
  int selectedIndex = -1;
  Uint8List imageOfDrag = null;
  List<int> types = [];
  Point lastPoint;
  List<Metal> collisions = List();
  List<Metal> cloneCollisions = List();
  int selectedIndexByTouch = -1;
  double scale = 3;
  double lineBase = 0;
  Metal topBox;
  double minLeft;
  Offset LeftSide;
  Offset RightSide;
  double widthOfTruck;
  double heightOfTruck;

  Queue<HashMap<int, Point>> undoBox = new Queue<HashMap<int, Point>>();

  final _shapeHelper = ShapeHelper();

  final _streamShapes = StreamController<List<Widget>>.broadcast();
  final _streamTruck = StreamController<List<Widget>>.broadcast();
  final _streamSelectedShape = StreamController<Metal>.broadcast();
  final _streamLoading = StreamController<bool>.broadcast();

  Stream<bool> get streamLoading => _streamLoading.stream;
  Stream<List<Widget>> get streamShapes => _streamShapes.stream;
  Stream<List<Widget>> get streamTruck => _streamTruck.stream;
  Stream<Metal> get streamSelectedShape => _streamSelectedShape.stream;

  Future<void> getShapes(String order) async {
    this.order = order;
    final orderHelper = await _shapeHelper.getShapesToEdit(order);
    orderHelper.fold((l) => null, (r) {
      metals = r;
      init(metals);
      renderTruck();
    });
  }

  Future<void> getResetShape() async {
    undoBox = new Queue<HashMap<int, Point>>();
    _streamShapes.add(null);
    _streamSelectedShape.add(null);
    final orderHelper = await _shapeHelper.getResetShape(order);
    orderHelper.fold((l) => null, (r) {
      metals = r;
      init(metals);
    });
  }

  reset() {
    selectedIndexByTouch = -1;
    _streamSelectedShape.add(null);
    undoBox = new Queue<HashMap<int, Point>>();
  }

  getSelectedMetal() {
    if (selectedIndexByTouch >= 0) {
      return metals[selectedIndexByTouch];
    }
    if (selectedIndex > 0) {
      return metals[selectedIndex];
    }

    return null;
  }

  removeSelectedWidget() {
    updateWidget();
  }

  init(List<Metal> metals) {
    initMetals = [];
    widthOfTruck = truck.width * 20 / 2000 * 300;
    heightOfTruck = truck.height * 20 / 2000 * 300;
    double minMaxOfLenth = Util.getWidthOfBloc(metals);
    minLeft = (SizeConfig.screenWidth / 13 * 6 - widthOfTruck) / 2;
    minLeft += (widthOfTruck - minMaxOfLenth) / 2;
    metals.forEach((element) {
      element.posX += minLeft;
      initMetals.add(Metal.clone(element));
    });
    lineBase = Util.getLineBase(metals);
    topBox = Util.getTopBox(metals);
    updateWidget();
  }

  getCurrentBox(double x, double y) {
    collisions = [];
    // Util.calRowNumber(metals);
    _streamSelectedShape.add(null);
    for (var i = 0; i < metals.length; i++) {
      if (metals[i].isInner(x, y)) {
        selectedIndex = i;
        lastPoint = Point(metals[i].posX, metals[i].posY);
        HashMap map = HashMap<int, Point>();
        Point point = Point(metals[i].posX, metals[i].posY);
        map[selectedIndex] = point;
        undoBox.add(map);
        return i;
      }
    }
    return -1;
  }

  updatePositions(double angle, double x, double y, BuildContext context) {
    selectedIndexByTouch = -1;
    dashLines = [];
    collisions = [];
    cloneCollisions = [];
    Size size = MediaQuery.of(context).size;
    double HEIGHT = lineBase;
    double WIDTH = size.width / 13 * 6;
    if (selectedIndex >= 0) {
      Metal metal = metals[selectedIndex];
      double dx = x;
      double dy = y;
      for (var i = 0; i < metals.length; i++) {
        if ((metals[i].posX - metal.posX).abs() <= 10 &&
            metals[i].posX != metal.posX) {
          dashLines.add(Point(metals[i].posX, metals[i].posY));
        }
        if ((metals[i].right - metal.posX).abs() <= 10) {
          dashLines.add(Point(metals[i].right, metals[i].posY));
        }

        if ((metals[i].right - metal.right).abs() <= 10 &&
            metals[i].right != metal.right) {
          dashLines.add(Point(metals[i].right, metals[i].posY));
        }
        types.add(metals[i].type);
        if (metal.checkCollision(metals[i])) {
          // collisions.add(metals[i]);
          Metal clone = Metal.clone(metals[i]);
          cloneCollisions.add(clone);
        } else {
          // metals[i].type = types[i];
        }
      }
      double min;
      if (dashLines.length > 0) {
        min = dashLines[0].y;
        dashLines.forEach((element) {
          if (element.y < min) {
            min = element.y;
          }
        });
        dashLines.removeWhere((element) => element.y > min);
      }
      cloneCollisions.forEach((element) {
        getSide(metal, element);
      });
      metal.posX += dx;
      metal.posY += dy;
      if (metal.posX <= LeftSide.dx) metal.posX = LeftSide.dx;
      if (metal.posX >= RightSide.dx - metal.width)
        metal.posX = RightSide.dx - metal.width;
      if (metal.posY <= 0) metal.posY = 0;
      if (metal.posY >= HEIGHT - metal.height)
        metal.posY = HEIGHT - metal.height;

      updateWidget();
      dashLines.forEach((e) {
        items.add(CustomPaint(
          painter: LineDashedPainter(e.x, e.y),
        ));
      });
    }
  }

  getSide(Metal metal, Metal element) {
    double b_collision = element.bottom - metal.posY;
    double t_collision = metal.bottom - element.posY;
    double l_collision = metal.right - element.posX;
    double r_collision = element.right - metal.posX;

    if (t_collision < b_collision &&
        t_collision < l_collision &&
        t_collision < r_collision) {
      // metal.posY = element.posY - metal.height;

//Top collision
    }
    if (b_collision < t_collision &&
        b_collision < l_collision &&
        b_collision < r_collision) {
//bottom collision
//       metal.posY = element.bottom;
    }
    if (l_collision < r_collision &&
        l_collision < t_collision &&
        l_collision < b_collision) {
//Left collision
//       metal.posX = element.posX - metal.width;
      print('left collision');
      double temp = metal.posX;
      metal.posX  =element.right;
      element.posX = temp - metal.width;
    }
    if (r_collision < l_collision &&
        r_collision < t_collision &&
        r_collision < b_collision) {
//Right collision
      metal.posX = element.right;
    }
  }

  onEnd() {
    if (selectedIndex == -1) return;
    Metal metal = metals[selectedIndex];
    cloneCollisions.forEach((element) {
      getSide(
        metal,
        element,
      );
    });

    if (dashLines.length > 0) {
      dashLines.forEach((element) {
        if ((element.x - metal.posX).abs() <= 10 && element.x != metal.posX) {
          metal.posX = element.x;
        }
        if ((element.x - metal.right).abs() <= 10 && element.x != metal.right) {
          metal.posX = element.x - metal.width;
        }
      });
    }

    bool col = true;
    while (col) {
      metal.posY += 0.2;
      for (var i = 0; i < metals.length; i++) {
        if (metal.checkCollision(metals[i]) &&
            metals[i].checkCollision(metal)) {
          col = false;
          break;
        }
      }
      if (metal.posY >= lineBase - metal.height) {
        break;
      }
      updateWidget();
    }
    selectedIndex = -1;
    types = [];
  }

  undo() {
    if (undoBox.length > 0) {
      _streamSelectedShape.add(null);
      HashMap map = undoBox.last;
      undoBox.removeLast();
      map.forEach((key, value) {
        metals[key].posY = value.y;
        metals[key].posX = value.x;
      });
      updateWidget();
    }
  }

  renderTruck() {
    trucks = [];
    widthOfTruck = truck.width * 20 / 2000 * 300;
    heightOfTruck = truck.height * 20 / 2000 * 300;
    LeftSide =
        Offset((SizeConfig.screenWidth / 13 * 6 - widthOfTruck) / 2, lineBase);
    RightSide =
        Offset((SizeConfig.screenWidth / 13 * 6 - LeftSide.dx), lineBase);
    trucks.add(CustomPaint(
      painter: LineDashedPainter(LeftSide.dx, LeftSide.dy),
    ));
    trucks.add(CustomPaint(
      painter: LineDashedPainter(RightSide.dx, RightSide.dy),
    ));
    trucks.add(CustomPaint(
      painter: DrawDashLine(Offset(LeftSide.dx, LeftSide.dy - heightOfTruck),
          Offset(RightSide.dx, RightSide.dy - heightOfTruck)),
    ));
    trucks.add(Positioned(
      left: LeftSide.dx,
      top: 0,
      child: CustomPaint(
        painter: TruckPainterTrailToEdit(
            truck, 2.3, widthOfTruck / 2.3, heightOfTruck),
      ),
    ));
    _streamTruck.add(trucks);
  }

  updateWidget() {
    items = [];
    topBox = Util.getTopBox(metals);

    List<Widget> shims = [];
    List<Widget> shapes = [];
    List<Widget> covers = [];

    List<Point> lines = getLine(metals);
    List<Point> listPointTopLeft = List<Point>();
    List<Point> listPointTopRight = List<Point>();

    metals.forEach((element) {
      shapes.add(genWidget(element, Offset(0, 0), 1, true));
      shims.add(genShim(element, lineBase));
      covers.addAll(genPads(element));
      Point pointLeft = Point(element.left, element.top);
      Point pointRight = Point(element.right, element.top);
      if (lines.contains(pointLeft)) listPointTopLeft.add(pointLeft);
      if (lines.contains(pointRight)) listPointTopRight.add(pointRight);
    });

    items.addAll(genLeftPad(listPointTopLeft, topBox.posX));
    items.addAll(genRightPad(listPointTopRight, topBox.posX));
    items.add(genPath(lines, lineBase));
    items.addAll(shims);
    items.addAll(shapes);
    items.addAll(covers);
    _streamShapes.add(items);
  }

  changeCorner(Offset touch) {
    // Util.calRowNumber(metals);
    int index = -1;
    for (var i = 0; i < metals.length; i++) {
      if (metals[i].isInner(touch.dx, touch.dy)) {
        index = i;
        break;
      }
    }
    if (index != -1) {
      selectedIndexByTouch = index;
      Metal metal = metals[index];
      _streamSelectedShape.add(metal);
      return metal;
    } else {
      selectedIndexByTouch = -1;
      _streamSelectedShape.add(null);
      return null;
    }
  }

  addVeritcalWood(int position) {
    addVerticalWoodChanged = true;
    Metal metal = metals[selectedIndexByTouch];
    if (metal.pads.contains(position)) {
      metal.pads.remove(position);
    } else {
      metal.pads.add(position);
    }
    updateWidget();
  }

  checkVerticalWoodChanged() {
    String initStr = "";
    String str = "";
    for (var i = 0; i < initMetals.length; i++) {
      initStr += "$i:";
      str += "$i:";
      if (initMetals[i].pads.length > 0)
        initMetals[i].pads.sort((a, b) {
          if (a > b)
            return -1; //1 tang dan ; -1 giam dan
          else if (a < b)
            return 1;
          else
            return 0;
        });
      if (metals[i].pads.length > 0)
        metals[i].pads.sort((a, b) {
          if (a > b)
            return -1; //1 tang dan ; -1 giam dan
          else if (a < b)
            return 1;
          else
            return 0;
        });
      initMetals[i].pads.forEach((element) {
        initStr += element.toString();
      });
      metals[i].pads.forEach((element) {
        str += element.toString();
      });
      initStr += "+";
      str += "+";
    }
    if (initStr == str) return false;
    return true;
  }

  getPositionInnerRect(Offset offset, Metal metal) {
    double x = offset.dx;
    if ((metal.right - x).abs() <= 20) return 2;
    if ((metal.left - x).abs() <= 20) {
      return 4;
    }
    return -1;
  }

  canSave() {
    if (topBox.top < LeftSide.dy - heightOfTruck) return true;
    return false;
  }

  Future<bool> save() async {
    selectedIndexByTouch = -1;
    _streamSelectedShape.add(null);
    String body = "";
    metals.forEach((element) {
      Metal revert = Metal.revert(element, Configs.scaleToEdit, minLeft);
      body += revert.toJson() + ',';
    });
    body = Util.removeLastCharacter(body);
    body = "[ " + body + " ]";
    _streamShapes.add(null);
    final result = await _shapeHelper.updateShapes(body);
    return result.fold((l) {}, (r) {
      addVerticalWoodChanged = false;
      _streamShapes.add(items);
      // undoBox = new Queue<HashMap<int, Point>>();
      return true;
    });
  }

  Future<Uint8List> capturePng() async {
    try {
      RenderRepaintBoundary boundary =
          globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      imageOfDrag = byteData.buffer.asUint8List();
    } catch (e) {
      print(e);
    }
    return null;
  }
}

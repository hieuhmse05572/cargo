import 'package:HirohataCargo/src/models/record.dart';
import 'package:flutter/material.dart';

// Widget _rowTag(double fontsize, Record record, bool isHighlight, int index,
//     final controller) {
//   return AutoScrollTag(
//       key: ValueKey(index),
//       controller: controller,
//       index: index,
//       child: _row(fontsize, record, isHighlight));
// }
class buildRow extends StatelessWidget {
  final Record record;
  buildRow({this.record});
  @override
  Widget build(BuildContext context) {
    return _buildRow(record);
  }

  Widget _buildRow(Record record) {
    double fontsize = 11;
    return Container(
      height: 20,
      color:
          record.isHighlight ? Colors.red.withOpacity(0.4) : Colors.transparent,
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerRight,
                child: Text(
                    "${record.id < 10 ? '0' + record.id.toString() : record.id}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerRight,
                child: Text(
                    "${record.row_number.toInt() > 0 ? (record.row_number < 10 ? '0' + record.row_number.toInt().toString() : record.row_number.toInt()) : ""}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerRight,
                child: Text(
                    "${record.widthOfRow > 0 ? record.widthOfRow.toInt() : ""}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerRight,
                child: Text("${record.column_range}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerRight,
                child: Text("1", style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerRight,
                child: Text("${record.length.toInt()}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerRight,
                child: Text("${record.height.toInt()}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerRight,
                child: Text("${record.width.toInt()}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 3,
            child: Container(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text("${record.cross_sectional_dim}",
                      style: TextStyle(fontSize: fontsize)),
                )),
          ),
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerRight,
                child: Text("${record.number_of_individuals}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerRight,
                child: Text("${record.duplicateN}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerRight,
                child: Text("0", style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 1,
            child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                    "${record.anointingClassification == '1' ? "塗装" : "油有"}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 2,
            child: Container(
                alignment: Alignment.centerLeft,
                child: Text("${record.material_name}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 2,
            child: Container(
                alignment: Alignment.centerLeft,
                child: Text("${record.specific_product}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 3,
            child: Container(
                alignment: Alignment.centerLeft,
                child: Text("K5674", style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 3,
            child: Container(
                alignment: Alignment.centerLeft,
                child: Text(
                    "${record.invoice_shipping_location}${record.invoice_transaction_type}${record.invoice_serial_number}${record.invoice_line_number}",
                    style: TextStyle(fontSize: fontsize))),
          ),
          Expanded(
            flex: 3,
            child: Container(
                alignment: Alignment.centerLeft,
                child: Text("${record.contract_number}",
                    style: TextStyle(fontSize: fontsize))),
          ),
        ],
      ),
    );
  }
}

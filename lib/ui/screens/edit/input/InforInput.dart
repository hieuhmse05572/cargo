import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class InforInput extends StatefulWidget {
  final Infor infor;
  InforInput({this.infor});
  @override
  _CustomInputState createState() => _CustomInputState();
}

class _CustomInputState extends State<InforInput> with AfterLayoutMixin {
  final provider = getIt<DrawProvider>();

  TextEditingController textEditingController1 = TextEditingController();
  TextEditingController textEditingController2 = TextEditingController();
  bool topVal = false;
  bool botVal = false;

  @override
  Widget build(BuildContext context) {
    // final dataModel = Provider.of<DrawPadBloc>(context, listen: false);
    // if(provider.initLoading) {

    // initLoading = false;
    // }
    return Container(
      margin: EdgeInsets.only(right: 30, top: 20, bottom: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text("マンボ "),
              Container(
                width: 90,
                height: 30,
                child: TextFormField(
                  controller: textEditingController1,
                  // initialValue:
                  // dataModel.infor == null ? "" : dataModel.infor.manbo,
                  onChanged: (text) {
                    provider.updateInput(1, "$text");
                    // textEditingController1.text = text + " 本";
                  },
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly
                  ],
                  keyboardType: TextInputType.numberWithOptions(decimal: true),

                  decoration: new InputDecoration(
                    suffixText: " 本",
                    counterText: "",
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 0.0, horizontal: 10.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.greenAccent, width: 1.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0),
                    ),
                  ),
                  maxLength: 5,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 3,
            width: 1,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text("固縛 "),
              Container(
                width: 90,
                height: 30,
                child: TextFormField(
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly
                  ],
                  controller: textEditingController2,
                  onChanged: (text) {
                    provider.updateInput(2, "$text");
                    // textEditingController2.text = text + " 点";
                  },
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  decoration: new InputDecoration(
                    suffixText: " 点",
                    counterText: "",
                    contentPadding: new EdgeInsets.symmetric(
                        vertical: 0.0, horizontal: 10.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.greenAccent, width: 1.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0),
                    ),
                  ),
                  maxLength: 5,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    textEditingController1.text =
        widget.infor == null ? "" : widget.infor.manbo;
    textEditingController2.text =
        widget.infor == null ? "" : widget.infor.kobaku;
    // TODO: implement afterFirstLayout
  }
}

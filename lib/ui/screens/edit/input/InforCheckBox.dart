import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:flutter/material.dart';

class InforCheckBox extends StatefulWidget {
  final Infor infor;
  InforCheckBox({this.infor});
  @override
  _InforCheckBoxState createState() => _InforCheckBoxState();
}

class _InforCheckBoxState extends State<InforCheckBox> {
  final provider = getIt<DrawProvider>();

  @override
  Widget build(BuildContext context) {
    bool topVal = widget.infor == null ? false : widget.infor.sutashon;
    bool botVal =
    widget.infor == null ? false : widget.infor.hakka_oroshi;
    return Column(
      children: [
        Container(
          height: 20,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Checkbox(
                onChanged: (bool value) {
                  setState(() {
                    topVal = value;
                  });
                  if (value)
                    provider.updateCheckBox(1, true);
                  else
                    provider.updateCheckBox(1, false);
                },
                value: topVal,
              ),
              Text("ｽﾀﾝｼｮﾝ "),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Checkbox(
              onChanged: (bool value) {
                setState(() {
                  botVal = value;
                });
                if (value)
                  provider.updateCheckBox(2, true);
                else
                  provider.updateCheckBox(2, false);
              },
              value: botVal,
            ),
            Text("ﾊｯｶｰ卸 "),
          ],
        ),
      ],
    );
  }
}

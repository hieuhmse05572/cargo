import 'package:HirohataCargo/src/models/truck.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'messages.dart';

// class EditTruck extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     throw UnimplementedError();
//   }
// }

dialogChangeTruck(BuildContext context) {
  return Dialog(
//            contentPadding: EdgeInsets.all(5),
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      width: 420,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                '車 種',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
              IconButton(
                icon: Icon(
                  Icons.clear,
                  color: Colors.purple,
                ),
                onPressed: () {
                  Navigator.pop(context, "0");
                },
              )
            ],
          ),
          Divider(),
          SizedBox(
            height: 5,
            width: 1,
          ),
          SizedBox(
              height: 240,
              child: ListView(
                children: [
                  _buildRow(context, Constants.truck4t),
                  _buildRow(context, Constants.truck10t),
                  _buildRow(context, Constants.truckContainer)
                ],
              ))
        ],
      ),
    ),
  );
}

Widget _buildRow(BuildContext context, Truck truck) {
  return InkWell(
    onTap: () {
      Navigator.pop(context, truck.id);
    },
    child: Card(
      child: ListTile(
        leading: Icon(Icons.directions_car),
        title: Text(truck.name),
        subtitle: Text(
            "備 考: ${truck.length * 20}, 製 品 幅: ${truck.width * 20} , 高 さ: ${truck.height * 20}"),
        // trailing: Icon(Icons.),
      ),
    ),
  );
}

class DialogUtil {
  static Future<String> showDialogChangeTruck(BuildContext context) async {
    String text = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogChangeTruck(context);
      },
    );
    return text;
  }

  static Future<int> showDialogConfirm(BuildContext context, String msg) async {
    int result = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogConfirm(context, msg);
      },
    );
    return result;
  }

  static showMessages(BuildContext context, String text) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogMessage(context, text);
      },
    );
  }
}

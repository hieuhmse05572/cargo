import 'dart:math';

import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/ui/painter/renderCover.dart';
import 'package:HirohataCargo/ui/painter/renderLine.dart';
import 'package:HirohataCargo/ui/painter/renderPad.dart';
import 'package:flutter/material.dart';

import 'renderBox.dart';

Widget genPath(List<Point> lines, double lineBase) {
  return Positioned(
      left: 0,
      top: 0,
      child: CustomPaint(
        painter: LinePainter(lines, 1, lineBase),
      ));
}

List<Widget> genRightPad(List<Point> point, double topBox) {
  List<Widget> padsWidget = [];
  point.forEach((element) {
    if (element.x >= topBox) {
      padsWidget.add(Positioned(
        left: 0,
        top: 0,
        child: CustomPaint(painter: PadTopRightPainter(element, 1)),
      ));
    }
  });
  return padsWidget;
}

List<Widget> genLeftPad(List<Point> point, double topBox) {
  List<Widget> padsWidget = [];
  point.forEach((element) {
    if (element.x <= topBox) {
      padsWidget.add(Positioned(
        left: 0,
        top: 0,
        child: CustomPaint(painter: PadTopLeftPainter(element, 1)),
      ));
    }
  });
  return padsWidget;
}

Widget genShim(Metal metal, double lineBase) {
  // if (metal.bottom != lineBase) {
  return CustomPaint(
    painter: BoxShimPainter(
        Offset(metal.left, metal.bottom), Offset(metal.right, lineBase)),
  );
  // }
}

List<Widget> genPads(Metal metal) {
  List<Widget> covers = [];
  metal.pads.forEach((pad) {
    covers.add(CustomPaint(
      painter: CoverPainter(metal: metal, position: pad, scale: 1),
    ));
  });
  return covers;
}

Widget genWidget(Metal metal, Offset offset, double scale, bool canEdit) {
  Widget widget = Container();
  if (metal.type == 1)
    widget = Positioned(
      top: offset.dy,
      left: offset.dx,
      child: CustomPaint(
        painter: BoxPainter1(metal, scale, canEdit),
      ),
    );
  else if (metal.type == 2)
    widget = Positioned(
      top: offset.dy,
      left: offset.dx,
      child: CustomPaint(
        painter: BoxPainter2(metal, scale, canEdit),
      ),
    );
  // if (metal.type == 5 && metal.height != 0){
  //   widget = CustomPaint(
  //     painter: BoxPainterOnDrag(
  //         Rect.fromLTWH(metal.left, metal.top, metal.width, metal.height)),
  //   );}
  return widget;
}

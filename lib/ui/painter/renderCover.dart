import 'dart:math';

import 'package:HirohataCargo/src/models/metal.dart';
import 'package:flutter/material.dart';

class CoverPainter extends CustomPainter {
  final Metal metal;
  int position = 1;
  double scale;
  CoverPainter({this.metal, this.position, this.scale});
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.amber
      ..strokeWidth = 5
      ..strokeCap = StrokeCap.butt
      ..style = PaintingStyle.stroke;
    Path path = new Path();
    // 1 top
    // 2 right
    // 3 bottom
    // 4 left
    if (position == 3) {
      path.moveTo(metal.left * scale, metal.bottom * scale);
      path.lineTo(metal.right * scale, metal.bottom * scale);
      canvas.drawPath(path, paint);
    } else if (position == 1) {
      path.moveTo(metal.left * scale, metal.top * scale);
      path.lineTo(metal.right * scale, metal.top * scale);
      canvas.drawPath(path, paint);
    } else if (position == 2) {
      path.moveTo(metal.right * scale, metal.top * scale + 2 * scale);
      path.lineTo(metal.right * scale, metal.bottom * scale - 2 * scale);
      canvas.drawPath(path, paint);
    } else {
      path.moveTo(metal.left * scale, metal.top * scale + 2 * scale);
      path.lineTo(metal.left * scale, metal.bottom * scale - 2 * scale);

      canvas.drawPath(path, paint);
    }

    // canvas.drawLine(startingPoint, midPoint, paint);
    // canvas.drawLine(midPoint, endingPoint, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}


import 'dart:math';

import 'package:HirohataCargo/src/utils/utils.dart';
import 'package:flutter/material.dart';

class LinePainter extends CustomPainter {
  final List<Point> points;
  final double scale;
  final double baseLine;
  LinePainter(this.points, this.scale, this.baseLine);
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = 2
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;
    Path path = new Path();
    path.moveTo(points[0].x* scale, points[0].y * scale);
    // path.lineTo(posX + 40, posY);
    points.forEach((element) {
      path.lineTo(element.x* scale, element.y* scale);
    });
    // path.lineTo(Config.bottomRight.x, Config.bottomRight.y);
    path.close();
    canvas.drawPath(path, paint);
    // canvas.drawLine(
    //     Offset(0, Config.size.height / 6 * 4), Offset(), paint);
    // canvas.drawLine(midPoint, endingPoint, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
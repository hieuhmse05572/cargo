import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BackgroundPainter extends CustomPainter {
  BackgroundPainter(this.size, this.scale, this.offset);
  final Size size;
  final double scale;
  final Offset offset;

  @override
  void paint(Canvas canvas, Size size) {
    var paintFatLine = Paint()
      ..strokeWidth = 0.4
      ..color = Colors.grey.withOpacity(0.86)
      ..style = PaintingStyle.stroke;

    var paintThinLine = Paint()
      ..strokeWidth = 0.2
      ..color = Colors.grey.withOpacity(0.6)
      ..style = PaintingStyle.stroke;
    double root = -400;
    double line = -400;

    int count = 1;
    while (line < this.size.height || line < this.size.width) {
      line += 5 * scale;
      if (count == 4) {
        count = 0;
        canvas.drawLine(
            Offset(line, root), Offset(line, this.size.width), paintFatLine);
        canvas.drawLine(
            Offset(root, line), Offset(this.size.height, line), paintFatLine);
      } else {
        canvas.drawLine(
            Offset(line, root), Offset(line, this.size.width), paintThinLine);
        canvas.drawLine(
            Offset(root, line), Offset(this.size.height, line), paintThinLine);
      }
      count++;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

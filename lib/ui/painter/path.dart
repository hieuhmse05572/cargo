import 'dart:math';

import 'package:HirohataCargo/src/models/box.dart';
import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/utils/utils.dart';

import 'covexHux.dart';

Metal getTopBox(List<Metal> metals) {
  Metal metal = metals[0];
  metals.forEach((element) {
    if (element.posY <= metal.posY) {
      metal = element;
    }
  });
  return metal;
}

Point getTopLeftPoint(List<Metal> metals, Metal topBox) {
  Point point = Point(topBox.posX, topBox.posY);
  metals.forEach((element) {
    if (element.posY == point.y && element.posX <= point.x) {
      point = Point(element.posX, element.posY);
    }
  });
  return point;
}

Point getTopRightPoint(List<Metal> metals, Metal topBox) {
  Metal metal;
  Point point = Point(topBox.posX, topBox.posY);
  metals.forEach((element) {
    if (element.posY == point.y && element.posX >= point.x) {
      point = Point(element.posX, element.posY);
      metal = element;
    }
  });
  point = Point(point.x + metal.width, point.y);
  return point;
}

List<Point> getRightLine(List<Metal> metals) {
  List<Box> boxes = new List();
  metals.forEach((element) {
    boxes.add(Box(element.height, element.width, element.posX, element.posY));
  });
  boxes.sort((a, b) {
    if (a.posY > b.posY)
      return 1; //1 tang dan ; -1 giam dan
    else if (a.posY < b.posY)
      return -1;
    else
      return 0;
  });
  List<Point> p = List();
  Metal topBox = getTopBox(metals);
  double baseLine = Util.getLineBase(metals);
  Point botomRight = Util.getBottomRightPoint(metals, baseLine);
  Point boxTopRight = getTopRightPoint(metals, topBox);
  p.add(getTopLeftPoint(metals, topBox));
  p.add(boxTopRight);
  Point center = Point(boxTopRight.x, baseLine);

  bool exist = true;
  while (exist) {
    exist = false;
    center = Point(p.last.x, botomRight.y);
    for (var i = 0; i < boxes.length; i++) {
      Point point = Point(boxes[i].posX + boxes[i].width, boxes[i].posY);
      if (!inTrigon(point, botomRight, center, p.last) &&
          point.x > p.last.x &&
          point.y > p.last.y) {
        p.add(point);
        exist = true;
        break;
      }
    }
  }
  p.add(botomRight);
  return p;
}

List<Point> getLeftLine(List<Metal> metals) {
  List<Box> boxes = new List();
  metals.forEach((element) {
    boxes.add(Box(element.height, element.width, element.posX, element.posY));
  });
  boxes.sort((a, b) {
    if (a.posY > b.posY)
      return 1; //1 tang dan ; -1 giam dan
    else if (a.posY < b.posY)
      return -1;
    else
      return 0;
  });
  List<Point> p = List();
  Metal topBox = getTopBox(metals);
  double baseLine = Util.getLineBase(metals);
  Point botomLeft = Util.getBottomLeftPoint(metals, baseLine);
  Point boxTopLeft = getTopLeftPoint(metals, topBox);
  p.add(boxTopLeft);
  Point center = Point(boxTopLeft.x, baseLine);

  bool exist = true;
  while (exist) {
    exist = false;
    List<Point> tempPoint = List<Point>();
    center = Point(p.last.x, botomLeft.y);
    // print('---------------------');
    for (var i = 0; i < boxes.length; i++) {
      Point point = Point(boxes[i].posX, boxes[i].posY);
      if (!inTrigon(point, botomLeft, center, p.last) &&
          point.x < p.last.x &&
          point.y > p.last.y) {
        // print('in trigon: ');
        // print(point);
        p.add(point);
        exist = true;
        break;
      }
    }
  }
  // p.removeAt(0);
  p.add(botomLeft);
  return p;
}

//
List<Point> getLine(List<Metal> metals) {
//   // getRightLine(metals);
  double baseLine = Util.getLineBase(metals);
  List<Point> points = new List();
  metals.forEach((element) {
    points.add(Point(element.posX, element.posY));
    points.add(Point(element.right, element.top));
    points.add(Point(element.left, element.bottom));
    points.add(Point(element.right, element.bottom));
  });
  double min = Util.getMinOfBloc(metals);
  double max = Util.getWidthOfBloc(metals);
  points.add(Point(min, baseLine));
  points.add(Point(max, baseLine));
  return GFG.convexHull(points, points.length);
//   points.addAll(getRightLine(metals).reversed);
//   points.addAll(getLeftLine(metals));
//   return points;
}

bool inTrigon(Point s, Point a, Point b, Point c) {
  double asx = s.x - a.x;
  double asy = s.y - a.y;

  bool sab = (b.x - a.x) * asy - (b.y - a.y) * asx > 0.0;

  if ((c.x - a.x) * asy - (c.y - a.y) * asx > 0.0 == sab) return false;

  if ((c.x - b.x) * (s.y - b.y) - (c.y - b.y) * (s.x - b.x) > 0.0 != sab)
    return false;

  return true;
}

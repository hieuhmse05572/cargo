
import 'package:flutter/material.dart';

class TextUtil {
  static drawText(Canvas context,
      double name,
      double x,
      double y,double scale) {
    // context.save();
    // context.translate(0, 0);
    // context.rotate(angleRotationInRadians);
    double num = name/1000;
    TextSpan span = new TextSpan(
        style: new TextStyle(
            color: Colors.blue[800],
            fontSize: scale,
            // fontWeight: FontWeight.bold,
            fontFamily: 'Roboto'),
        text: "$num");
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context, new Offset(x, y));
    // context.restore();
  }
  static drawTextWitFontSize(Canvas context,
      double name,
      double fontSize,
      double x,
      double y,) {
    // context.save();
    // context.translate(0, 0);
    // context.rotate(angleRotationInRadians);
    double num = name/1000;
    TextSpan span = new TextSpan(
        style: new TextStyle(
            color: Colors.red,
            fontSize: fontSize,
            // fontWeight: FontWeight.bold,
            fontFamily: 'Roboto'),
        text: "$num");
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context, new Offset(x, y));
    // context.restore();
  }

  static drawTextCenter(Canvas context,
      double name,
      double x,
      double y,
      double size) {
    // context.save();
    double num = name/1000;

    // context.translate(0, 0);
    // context.rotate(angleRotationInRadians);
    TextSpan span = new TextSpan(
        style: new TextStyle(
            color: Colors.brown,
            fontSize: size,
            fontWeight: FontWeight.bold,
            ),
        text: "$num");
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context, new Offset(x, y));
    // context.restore();
  }
  static drawTextInfor(Canvas context,
      String name,
      double x,
      double y,) {
    // context.save();

    // context.translate(0, 0);
    // context.rotate(angleRotationInRadians);
    TextSpan span = new TextSpan(
        style: new TextStyle(
          color: Colors.black,
          fontSize: 13,
          fontWeight: FontWeight.bold,
        ),
        text: name);
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context, new Offset(x, y));
    // context.restore();
  }

}
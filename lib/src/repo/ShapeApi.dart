import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/utils/NetworkUtil.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart';

class ShapeApi {
  Future<Either<Exception, Response>> getShapes(String order) async {
    try {
      final url = NetworkUtil.BASE_URL +
          '/api/shapeFilterProductCode?productCode=$order';
      Response response =
          await post(url, headers: NetworkUtil.getRequestHeaders()).timeout(
        Duration(seconds: 10),
      );
      return Right(response);
    } catch (e) {
      return Left(e);
    }
  }

  Future<Either<Exception, Response>> getResetShape(String order) async {
    try {
      final url = NetworkUtil.BASE_URL +
          '/api/sortByProductCodeAPIController?productCode=$order';
      Response response =
          await post(url, headers: NetworkUtil.getRequestHeaders()).timeout(
        Duration(seconds: 10),
      );
      return Right(response);
    } catch (e) {
      return Left(e);
    }
  }

  Future<Either<Exception, Response>> updateShapes(String body) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/update';

      Response response =
          await post(url, headers: NetworkUtil.getRequestHeaders(), body: body)
              .timeout(const Duration(seconds: 5));
      return Right(response);
    } catch (e) {
      return Left(e);
    }
  }
  // Future<bool> updateShapes(List<Metal> metals) {}
}

import 'package:HirohataCargo/src/models/truck.dart';
import 'package:HirohataCargo/src/utils/NetworkUtil.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart';

class TruckApi {
  Future<Either<Exception, Response>> getTruckType(String order) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/getCarType?productCode=$order';
      Response response =
          await post(url, headers: NetworkUtil.getRequestHeaders()).timeout(
        Duration(seconds: 10),
      );
      return Right(response);
    } catch (e) {
      return Left(e);
    }
  }

  Future<Either<Exception, Response>> updateTruckType(
      String order, String type) async {
    try {
      final url = NetworkUtil.BASE_URL +
          '/api/updateCar?productCode=$order&carType=$type';
      Response response =
          await post(url, headers: NetworkUtil.getRequestHeaders()).timeout(
        Duration(seconds: 10),
      );
      return Right(response);
    } catch (e) {
      return Left(e);
    }
  }
}

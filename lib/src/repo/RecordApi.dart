import 'package:HirohataCargo/src/utils/NetworkUtil.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart';

class RecordApi {
  Future<Either<Exception, Response>> getRecords(String order) async {
    try {
      final url = NetworkUtil.BASE_URL +
          '/api/getRecordByCode?productCode=$order';
      Response response =
      await post(url, headers: NetworkUtil.getRequestHeaders()).timeout(
        Duration(seconds: 10),
      );
      return Right(response);
    } catch (e) {
      return Left(e);
    }
  }

// Future<List<Metal>> getResetShapes() {}
// Future<bool> updateShapes(List<Metal> metals) {}
}

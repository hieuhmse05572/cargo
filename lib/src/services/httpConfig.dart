class Http {
  String token = "";
  static final Http _inst = Http._internal();

  static String HostURL = "https://test-api.nsmp-system.com/HirohataCargo";
  static String VisionApiURL =
      "https://vision.googleapis.com/v1/images:annotate?key=AIzaSyD0bhqf54qJnp3xbfSwsDakuxaugN0kLB4";

  Map<String, String> getRequestHeaders() {
    return {
      "Content-Type": "application/json",
      "Authorization": "${_inst.token}",
    };
  }

  Map<String, String> getRequestHeaders2(int length) {
    return {
      "Content-Type": "application/json",
      "Authorization": "${_inst.token}",
      "Content-Length": "$length"
    };
  }

  Http._internal();

  factory Http(String token) {
    if (token != "") _inst.token = token;
    return _inst;
  }

  getBody(var base64Image) {
    return {
      "requests": [
        {
          "image": {"content": "$base64Image"},
          "features": [
            {"type": "DOCUMENT_TEXT_DETECTION"}
          ],
          "imageContext": {
            "languageHints": ["ja-t-i0-handwrit"]
          }
        }
      ]
    };
  }
}

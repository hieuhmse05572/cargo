import 'dart:math';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;
  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;
  static Point topLeft;
  static Point topRight;
  static Point bottomLeft ;
  static Point bottomRight ;
  static var font;


  void init(BuildContext context){
    _mediaQueryData = MediaQuery.of(context);
    rootBundle
        .load("assets/fonts/MSMINCHO.TTF")
        .then((value){
          font = value;
    });
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    topLeft = Point(0.0, 0.0);
    topRight = Point(screenWidth / 13 * 6.0, 0);
    bottomLeft = Point(0.0, screenHeight / 6 * 4.0 - 100);
    bottomRight=  Point(screenWidth / 12 * 6.0, screenHeight / 6 * 4.0 - 100);
    blockSizeHorizontal = screenWidth/100;
    blockSizeVertical = screenHeight/100;
    _safeAreaHorizontal = _mediaQueryData.padding.left +
        _mediaQueryData.padding.right;
    _safeAreaVertical = _mediaQueryData.padding.top +
        _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal)/100;
    safeBlockVertical = (screenHeight - _safeAreaVertical)/100;
  }
}
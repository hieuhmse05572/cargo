class NetworkUtil {
  String TOKEN = "";
  static final NetworkUtil _inst = NetworkUtil._internal();

  static String BASE_URL = "https://test-api.nsmp-system.com/HirohataCargo";

  static Map<String, String>  getRequestHeaders() {
    return {
      "Content-Type": "application/json",
      "Authorization": "${_inst.TOKEN}",
    };
  }

  static Map<String, String> getRequestHeaders2(int length) {
    return {
      "Content-Type": "application/json",
      "Authorization": "${_inst.TOKEN}",
      "Content-Length": "$length"
    };
  }

  NetworkUtil._internal();

  factory NetworkUtil(String token) {
    if (token != "") _inst.TOKEN = token;
    return _inst;
  }
}

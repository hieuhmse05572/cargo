import 'package:flutter/material.dart';

class ColorsOf {
  static Color border1 = Color.fromRGBO(91, 155, 213, 1);
  static Color border2 = Color.fromRGBO(197, 90, 17, 1);
  static Color shim = Colors.brown.withOpacity(0.5);
  // static Color border2 = Colors.greenAccent;
  static Color borderline = Color.fromRGBO(68, 84, 106, 1);
  static Color search = Color(0xffe4eaf0);

  static List<Color> colors = [
    Color(0xff34495e),
    Color(0xff34495e),
    Color(0xff16a085),
    Color(0xffe67e22),
    Color(0xffe8307b),
    Color(0xff8e44ad),
    Color(0xffc0392b),
  ];
}

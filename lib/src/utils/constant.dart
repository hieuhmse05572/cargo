import 'package:HirohataCargo/src/models/truck.dart';

class Network {
  static String TIMEOUT = "0";
  static String SUCCESS = "200";
  static String ERROR = "500";
}

class Configs {
  static double scale = 1 / 20;
  static double scaleToEdit = 0.15;
  static double baseLine = 400;
}

class Msg {
  static String LoginSucess = "ログインが成功しました。";
  static String LoginFail = "ログインが失敗しました。";
  static String Timeout = "Error timeout";
}

class Messages {
  static String CHANGE_TRUCK = "選択した車類が適当ではありません。再度選択してください。";
  static String CONFIRM_SAVE = "保存します。よろしいですか？";
  static String CONFIRM_RESET = "リセットします。よろしいですか？";
  static String SAVE_SUCCESS = "保存が完了しました。";
  static String RESET_SUCCESS = "リセットが完了しました。";
  static String RESET_BACK = "入力した情報が保存されていない場合、入力情報は消えますがよろしいですか？";
  static String WARNING_HEIGHT = "車の高さを超えないようにもう一度お試してみてください。";

  static String SEARCH_EMPTY = "一致する検索結果がありません。";
}

class Constants {
  static Truck truck4t = Truck("1", "① 4 t 車", 2000, 1500, 6000);
  static Truck truck10t = Truck("2", "② 1 0 t 車", 2000, 1500, 9600);
  static Truck truckContainer = Truck("3", "③ ト レ ー ラ", 2500, 1500, 12000);

  static String sutansho = "赤・黒";
  static String verticalWood = "じん木追加";
  static String hakka_oroshi = "スタンション";
  static String drawing =
      '[ { "dx":138.5, "dy": 223.16666666666663 } ,{ "dx":141.5, "dy": 223.16666666666663 } ,{ "dx":148.5, "dy": 223.16666666666663 } ,{ "dx":158.0, "dy": 222.16666666666663 } ,{ "dx":166.5, "dy": 219.66666666666663 } ,{ "dx":172.5, "dy": 219.66666666666663 } ,{ "dx":176.0, "dy": 219.66666666666663 } ,{ "dx":177.5, "dy": 223.16666666666663 } ,{ "dx":179.0, "dy": 227.66666666666663 } ,{ "dx":180.0, "dy": 231.66666666666663 } ,{ "dx":182.5, "dy": 233.16666666666663 } ,{ "dx":187.5, "dy": 233.66666666666663 } ,{ "dx":195.5, "dy": 233.66666666666663 } ,{ "dx":204.5, "dy": 231.16666666666663 } ,{ "dx":213.5, "dy": 229.16666666666663 } ,{ "dx":221.0, "dy": 228.16666666666663 } ,{ "dx":230.5, "dy": 228.16666666666663 } ,{ "dx":235.0, "dy": 228.16666666666663 } ,{ "dx":240.5, "dy": 227.66666666666663 } ,{ "dx":143.0, "dy": 328.66666666666663 } ,{ "dx":145.5, "dy": 337.66666666666663 } ,{ "dx":150.5, "dy": 349.16666666666663 } ,{ "dx":160.0, "dy": 361.16666666666663 } ,{ "dx":171.0, "dy": 371.66666666666663 } ,{ "dx":187.5, "dy": 381.66666666666663 } ,{ "dx":207.0, "dy": 389.16666666666663 } ,{ "dx":224.0, "dy": 395.16666666666663 } ,{ "dx":71.0, "dy": 305.16666666666663 } ,{ "dx":71.0, "dy": 307.16666666666663 } ,{ "dx":70.5, "dy": 311.16666666666663 } ,{ "dx":68.5, "dy": 315.66666666666663 } ,{ "dx":66.5, "dy": 320.16666666666663 } ,{ "dx":65.5, "dy": 324.16666666666663 } ,{ "dx":64.5, "dy": 327.66666666666663 } ,{ "dx":63.5, "dy": 330.66666666666663 } ,{ "dx":63.0, "dy": 333.16666666666663 } ,{ "dx":63.0, "dy": 335.16666666666663 } ,{ "dx":63.0, "dy": 336.16666666666663 } ,{ "dx":62.5, "dy": 337.16666666666663 } ,{ "dx":62.5, "dy": 338.16666666666663 } ,{ "dx":62.0, "dy": 338.66666666666663 } ,{ "dx":61.5, "dy": 338.66666666666663 } ,{ "dx":60.0, "dy": 338.66666666666663 } ,{ "dx":58.5, "dy": 338.66666666666663 } ,{ "dx":55.0, "dy": 335.16666666666663 } ,{ "dx":-1.0, "dy": -1.0 } ,{ "dx":295.5, "dy": 307.66666666666663 } ,{ "dx":295.5, "dy": 311.16666666666663 } ,{ "dx":296.0, "dy": 314.66666666666663 } ,{ "dx":296.0, "dy": 319.16666666666663 } ,{ "dx":296.0, "dy": 325.16666666666663 } ,{ "dx":296.0, "dy": 330.66666666666663 } ,{ "dx":296.0, "dy": 334.66666666666663 } ,{ "dx":296.0, "dy": 337.16666666666663 } ,{ "dx":295.0, "dy": 339.16666666666663 } ,{ "dx":294.0, "dy": 340.66666666666663 } ,{ "dx":291.5, "dy": 341.16666666666663 } ,{ "dx":289.0, "dy": 341.66666666666663 } ,{ "dx":287.0, "dy": 342.66666666666663 } ,{ "dx":-1.0, "dy": -1.0 } ]';
}

class Button {
  static String btnBack = "戻る";
  static String btnPrint = "印刷 ";
  static String btnSave = "保存";
  static String btnReset = "再設定";
  static String btnUndo = "取り消す";
  static String btnRight = "右";
  static String btnLeft = "左";
  static String btnOk = "[はい」";
  static String btnCancel = "[いいえ]";
}

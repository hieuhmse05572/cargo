class Truck {
  String id;
  String name;
  double length;
  double width;
  double height;

  Truck(id, name, width, height, length) {
    this.id = id;
    this.name = name;
    this.width = width / 20;
    this.height = height / 20;
    this.length = length / 20;
  }
}

import 'dart:convert';

import 'package:HirohataCargo/src/models/glitch/NoInternetGlitch.dart';
import 'package:HirohataCargo/src/models/glitch/glitch.dart';
import 'package:HirohataCargo/src/repo/OrderApi.dart';
import 'package:HirohataCargo/src/models/order.dart' as order;
import 'package:dartz/dartz.dart';

class OrderHelper {
  final api = OrderApi();
  Future<Either<Glitch, List<order.Order>>> getOrders(String search) async {
    final apiResult = await api.getOrders();
    return apiResult.fold((l) {
      return Left(NoInternetGlitch());
    }, (r) {
      if (r.statusCode == 200) {
        final data = jsonDecode(r.body);
        if (data != null) {
          List<order.Order> orders = List();
          for (String i in data) {
            if (i.toLowerCase().contains(search.toLowerCase()) || search == "")
              orders.add(order.Order(name: i, date: ''));
          }
          return Right(orders);
        }
      }
      return Right([]);
    });
  }
}

import 'dart:convert';

import 'package:HirohataCargo/src/models/glitch/NoInternetGlitch.dart';
import 'package:HirohataCargo/src/models/glitch/glitch.dart';
import 'package:HirohataCargo/src/repo/LoginApi.dart';
import 'package:HirohataCargo/src/utils/NetworkUtil.dart';
import 'package:dartz/dartz.dart';

class LoginHelper {
  final api = LoginApi();
  Future<Either<Glitch, bool>> login(String user, String password) async {
    final apiResult = await api.login(user, password);
    return apiResult.fold((l) {
      return Left(NoInternetGlitch());
    }, (r) {
      if (r.statusCode == 200) {
        final data = jsonDecode(r.body);
        if (data['authenticationToken'] != null) {
          print(data['authenticationToken']);
          NetworkUtil(data['authenticationToken']);
          return Right(true);
        }
      }
      return Right(false);
    });
  }
}

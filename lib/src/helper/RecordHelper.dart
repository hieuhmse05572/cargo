import 'dart:convert';

import 'package:HirohataCargo/src/models/glitch/NoInternetGlitch.dart';
import 'package:HirohataCargo/src/models/glitch/glitch.dart';
import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/models/record.dart';
import 'package:HirohataCargo/src/repo/RecordApi.dart';
import 'package:HirohataCargo/src/repo/ShapeApi.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:dartz/dartz.dart';

class RecordHelper {
  final api = RecordApi();
  Future<Either<Glitch, List<Record>>> getRecords(String order) async {
    final apiResult = await api.getRecords(order);
    return apiResult.fold((l) {
      return Left(NoInternetGlitch());
    }, (r) {
      if (r.statusCode == 200) {
        final data = jsonDecode(r.body);
        if (data != null && data.length > 0) {
          List<Record> records = List();
          // int id = 1;
          for (Map i in data) {
            Record record = Record.fromJson(i);
            if (record != null) {
              // record.id = id;
              records.add(record);
              // id++;
            }
          }
          return Right(records);
        }
      }
      return Right([]);
    });
  }
}
